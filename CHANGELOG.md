## v3.4.0 (2023-12-05)
- Update Go version to 1.21.6 (!44)

## v3.3.0 (2023-12-05)
- Update Go version to 1.19.9 (!41)

## v3.2.0 (2022-23-11)
- Update report schema to version 15 (!38)

## v3.1.3 (2022-30-9)
- Update Golang to 1.18 (!34)

## v3.1.2 (2022-1-9)
- Add AnalyzerDetails to security report (!33)

## v3.1.1 (2022-9-8)
- Upgrade Go Version to 1.18.5 (!32)

## v3.1.0 (2022-30-5)
- Pass COVFUZZ_ADDITIONAL_ARGS to underline fuzzer engines (!30)
- Add Ruby fuzzer (!22)

## v3 (2022-11-05)
- Bump gitlab-cov-fuzz version to v3 (!29)

## v2.1.12 (2022-28-04)
- Adds Details field to vulnerability report (!25)

## v2.1.11 (2022-01-04)
- Make the exit code consistent (!23)

## v2.1.10 (2022-10-03)
- Update changelog (!21)
- Fixes JQF fuzzer (!20)
- Make the error reporting from the fuzzing job consistent (!19)

## v2.1.9 (2022-24-02)
- Release patch (!17)
- Add report for cwtriage tool with AFL (!16)
- Add cwtriage tool for fuzzing with AFL (!10)

## v2.1.8 (2022-16-02)
- Release with Golang 1.17.7 (!14)
- Update Golang to 1.17.7 (!13)

## v2.1.7 (2022-01-02)
- Add error message when corpus folder does not exist in artifacts (!12)

## v2.1.6 (2022-10-01)
- Fixes lint and pipeline issues (!9)
- Upload Corpus to the package registry (!8)

## v2.1.5 (2021-10-12)
- Upgrade Report JSON Schema Version to 14.0.4 (!7)

## v2.1.4 (2020-01-09)

### New Features
* Add Initial AFL support

<!-- ### Bug fixes -->

## v2.1.2 (2020-01-09)

### New Features
* Add javascript support

<!-- ### Bug fixes -->

## v2.1.1 (2020-01-09)

<!-- ### New Features -->

### Bug fixes
* python crash parsing

## v2.1.0 (2020-01-05)

### New Features
* Add pythonfuzz Support

<!-- ### Bug fixes -->

## v2.0.49 (2020-29-12)

### New Features
* Add jsfuzz Support

<!-- ### Bug fixes -->

## v2.0.48 (2020-16-12)

<!-- ### New Features -->

### Bug fixes
* artifacts already exist error.

## v2.0.47 (2020-30-11)

### New Features
* Auto inject version and commit and build time
* Add Package registry as a backend storage via `--use-registry`

<!-- ### Bug fixes -->

### Maintenance
* Upgrade Go Version to 1.15

## v2.0.45 (2020-11-10)

<!-- ### New Features -->

<!-- ### Bug fixes -->

### Maintenance
* Fix code lint errors 

## v2.0.44 (2020-11-10)

<!-- ### New Features -->

<!-- ### Bug fixes -->

### Maintenance
* Fix code lint errors 

## v2.0.40 (2020-10-14)

<!-- ### New Features -->

### Bug fixes
* javafuzz: exclude only core package from coverage instead of examples

<!-- ### Maintenance -->

## v2.0.39 (2020-10-14)

### New Features
* Add `COVFUZZ_ADDITIONAL_ARGS` environment variable

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.37 (2020-10-13)

<!-- ### New Features -->

### Bug fixes
* Remove `ref` from required flags

<!-- ### Maintenance -->

## v2.0.36 (2020-10-13)

<!-- ### New Features -->

### Bug fixes
* Fixing continuous-fuzzing graceful shutdowns only 

<!-- ### Maintenance -->

## v2.0.35 (2020-10-13)

<!-- ### New Features -->

### Bug fixes
* exclude the fuzzing engine code coverage when using javafuzz (https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz/-/issues/1)

<!-- ### Maintenance -->

## v2.0.34 (2020-10-02)

<!-- ### New Features -->

### Bug fixes
* bugfix2 in javafuzz jacocoagent location

<!-- ### Maintenance -->

## v2.0.32 (2020-10-02)

<!-- ### New Features -->

### Bug fixes
* Output Race condition
* bugfix in javafuzz

<!-- ### Maintenance -->

## v2.0.31 (2020-10-01)

### New Features
* support for javafuzz

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.30 (2020-09-29)

<!-- ### New Features -->

### Bug fixes
* Better parsing of `CI_SERVER_URL` and `CI_SERVER_PORT` - should now work for offline environments

<!-- ### Maintenance -->

## v2.0.29 (2020-09-28)

<!-- ### New Features -->

### Bug fixes
* Fix search of llvm-symbolizer

<!-- ### Maintenance -->

## v2.0.28 (2020-09-28)

### New Features
* Add stack symbolization for libfuzzer (via llvm-symbolizer)

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.27 (2020-09-24)

<!-- ### New Features -->

### Bug fixes
* bugfix in parent-child previous job cancellation logic

<!-- ### Maintenance -->

## v2.0.26 (2020-09-24)

### New Features
* Change priority of tokens: 1) PRIVATE_TOKEN 2) JOB_TOKEN 

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.25 (2020-09-22)

### New Features
* Add support for long-running async jobs (libfuzzer) 

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.24 (2020-09-18)

<!-- ### New Features -->

### Bug fixes
* Add Status to Scan object in json report

<!-- ### Maintenance -->

## v2.0.23 (2020-09-17)

<!-- ### New Features -->

<!-- ### Bug fixes -->

### Maintenance
* Use common library for report generation

## v2.0.22 (2020-08-09)

<!-- ### New Features -->

### Bug fixes
* Fix [urlencode](https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/2347#note_410581382) of job names with spaces.

### Maintenance
* Fix SAST related issues

## v2.0.21 (2020-08-30)

<!-- ### New Features -->

### Bug fixes
* fix version format in security report
* add cve,stacktrace,target to security report

<!-- ### Maintenance -->

## v2.0.20 (2020-08-30)

<!-- ### New Features -->

<!-- ### Bug fixes -->

### Maintenance
* Change cobra to urfave/cli
* Change log to logrus

## v2.0.19 (2020-08-27)

<!-- ### New Features -->

### Bug fixes
* Fix mapping CWE -> URL

<!-- ### Maintenance -->

## v2.0.18 (2020-08-27)

<!-- ### New Features -->

### Bug fixes
* fix previous crash->cwe mapping

<!-- ### Maintenance -->

## v2.0.17 (2020-08-27)

### New Features
* Map `Heap-buffer-overflow READ to CWE-125`

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.16 (2020-08-26)

### New Features
* Add basic Swift stack parsing and analysis

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.15 (2020-08-26)

### New Features
* Enable RUST_BACKTRACE=1
* Add basic Rust stack parsing and analysis

<!-- ### Bug fixes -->

<!-- ### Maintenance -->

## v2.0.14 (2020-08-25)

<!-- ### New Features -->

### Bug fixes
* Fix support for unknown crashes
* Add basic support for Java stack parsing

<!-- ### Maintenance -->

## v2.0.13 (2020-08-24)

### New Features
* JQF support

<!-- ### Bug fixes -->

### Maintenance
* fix .gitlab-ci.yml 


## v2.0.12 (2020-08-24)

<!-- ### New Features -->

### Bug fixes
* If CWE is not available use `crashType` for identifier `value` in json report.

<!-- ### Maintenance -->

## v2.0.11 (2020-08-19)

<!-- ### New Features -->

<!-- ### Bug fixes -->

### Maintenance
* Add CHANGELOG.md to gitlab-cov-fuzz (release) repository


## v2.0.10 (2020-08-16)

<!-- ### New Features -->

### Bug fixes
* Process `400` error from `%s/api/v4/projects/%s/jobs/artifacts/master/download?job=` api as corpus not found.
  This error returned when the job is running in the first time.
* Auto assign `CI_JOB_NAME` to `COVFUZZ_JOB_NAME`

<!-- ### Maintenance -->

## v2.0.9 (2020-08-09)

Changelog will be available from v2.0.9.
This release will serve as a skeleton for future releases.

### New Features
* Describe new features with links to MR where available

### Bug fixes
* Describe bug fixes with links to MR where available

### Maintenance
* Other stuff related to CI, tests, etc...
